package ru.tinkoff.exchange.rates.enumeration;

public enum Currency {
    AUD, GBP, KRW, SEK,
    BGN, HKD, MXN, SGD,
    BRL, HRK, MYR, THB,
    CAD, HUF, NOK, TRY,
    CHF, IDR, NZD, USD,
    CNY, ILS, PHP, ZAR,
    CZK, INR, PLN, EUR,
    DKK, JPY, RON, RUB,
    NONE;

    public static Currency fromString(String cur) {
        for (Currency currency : Currency.values()) {
            if (currency.name().equalsIgnoreCase(cur)) {
                return currency;
            }
        }
        return NONE;
    }
}