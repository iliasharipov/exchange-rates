package ru.tinkoff.exchange.rates.model;

import java.util.Objects;

public class ApiResponse {
    private String base;
    private String date;
    private RateObject rates;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiResponse that = (ApiResponse) o;
        return Objects.equals(base, that.base) &&
                Objects.equals(rates.getName(), that.rates.getName());
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "base='" + base + '\'' +
                ", date=" + date +
                ", rates=" + rates +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(base, rates.getName());
    }

    public String getDate() {
        return date;
    }

    public String getBase() {
        return base;
    }

    public RateObject getRates() {
        return rates;
    }
}