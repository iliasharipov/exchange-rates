package ru.tinkoff.exchange.rates.model;

import java.util.Objects;

public class RateObject {
    private final String name;
    private final double rate;

    public RateObject(String name, double rate) {
        this.name = name;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public double getRate() {
        return rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RateObject that = (RateObject) o;
        return Double.compare(that.rate, rate) == 0 &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, rate);
    }

    @Override
    public String toString() {
        return "RateObject{" +
                "name='" + name + '\'' +
                ", rate=" + rate +
                '}';
    }
}