package ru.tinkoff.exchange.rates.model;

import java.util.Objects;

public class ApiCache {
    private final String key;
    private final ApiResponse apiResponse;

    public ApiCache(String key, ApiResponse apiResponse) {
        this.key = key;
        this.apiResponse = apiResponse;
    }

    public String getKey() {
        return key;
    }

    public ApiResponse getApiResponse() {
        return apiResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiCache cache = (ApiCache) o;
        return Objects.equals(key, cache.key);
    }

    @Override
    public int hashCode() {

        return Objects.hash(key);
    }
}