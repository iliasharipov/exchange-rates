package ru.tinkoff.exchange.rates.app;

import ru.tinkoff.exchange.rates.dto.ExchangeOutput;
import ru.tinkoff.exchange.rates.enumeration.Currency;
import ru.tinkoff.exchange.rates.model.RateObject;
import ru.tinkoff.exchange.rates.task.ExchangeRatesTask;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static ru.tinkoff.exchange.rates.enumeration.Currency.NONE;

public class App {
    private static final String UNSUPPORTED_ISO_CURRENCY_CODE = "Unsupported ISO currency code";

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        while (true) {
            Currency fromCurrency = getCurrency(sc, "Enter from currency:");
            Currency toCurrency = getCurrency(sc, "Enter to currency:");
            try {
                // TODO: 09.02.2018 Наладка асинхронной работы
                ExecutorService executor = Executors.newSingleThreadExecutor();
                Future<ExchangeOutput> task = executor.submit(new ExchangeRatesTask(fromCurrency, toCurrency));
                while (!task.isDone()) {
                    System.out.print(" . ");
                    Thread.sleep(500);
                }
                ExchangeOutput output = task.get();
                if (output != null && output.getApiResponse() != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(output.getApiResponse().getBase());
                    sb.append(" => ");
                    RateObject rates = output.getApiResponse().getRates();
                    if (rates != null) {
                        sb.append(rates.getName());
                        sb.append(" : ");
                        sb.append(rates.getRate());
                    }
                    System.out.println(sb);

                }
            } catch (InterruptedException | ExecutionException e) {
                System.out.println("InterruptedException");
            }
        }
    }

    private static Currency getCurrency(Scanner sc, String s) {
        boolean isEnterToCurrency = true;
        Currency toCurrency = null;
        while (isEnterToCurrency) {
            System.out.println(s);
            String cur = sc.nextLine();
            toCurrency = Currency.fromString(cur);
            if (!toCurrency.equals(NONE)) {
                isEnterToCurrency = false;
            } else {
                System.out.println(UNSUPPORTED_ISO_CURRENCY_CODE);
            }
        }
        return toCurrency;
    }
}