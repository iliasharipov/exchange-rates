package ru.tinkoff.exchange.rates.dto;

import ru.tinkoff.exchange.rates.model.ApiResponse;

public class ExchangeOutput {
    private ApiResponse apiResponse;
    private String errorMessage;

    public ExchangeOutput(ApiResponse apiResponse) {
        this.apiResponse = apiResponse;
    }

    public ExchangeOutput(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ApiResponse getApiResponse() {
        return apiResponse;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}