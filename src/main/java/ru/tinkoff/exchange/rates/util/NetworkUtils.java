package ru.tinkoff.exchange.rates.util;

import java.io.IOException;
import java.net.InetAddress;

public class NetworkUtils {

    public static boolean isAvailableNetworkInterfaces() {
        try {
            return InetAddress.getByName("www.google.com").isReachable(1000);
        } catch (IOException e) {
            return false;
        }
    }
}
