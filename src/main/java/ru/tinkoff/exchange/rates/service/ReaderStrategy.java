package ru.tinkoff.exchange.rates.service;

import ru.tinkoff.exchange.rates.dto.ExchangeOutput;
import ru.tinkoff.exchange.rates.enumeration.Currency;
import ru.tinkoff.exchange.rates.model.ApiResponse;

public interface ReaderStrategy {
    ExchangeOutput read(Currency fromCurrency, Currency toCurrency);
}
