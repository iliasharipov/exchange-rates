package ru.tinkoff.exchange.rates.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.tinkoff.exchange.rates.dto.ExchangeOutput;
import ru.tinkoff.exchange.rates.enumeration.Currency;
import ru.tinkoff.exchange.rates.model.ApiResponse;
import ru.tinkoff.exchange.rates.model.RateObject;
import ru.tinkoff.exchange.rates.util.RatesDeserializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class HttpReaderStrategyImpl implements ReaderStrategy {
    @Override
    public ExchangeOutput read(Currency fromCurrency, Currency toCurrency) {
        URLBuilder urlBuilder = new URLBuilder("api.fixer.io");
        urlBuilder.setConnectionType("http");
        urlBuilder.addSubfolder("latest");
        urlBuilder.addParameter("base", fromCurrency.name());
        urlBuilder.addParameter("symbols", toCurrency.name());

        try {
            URL url = urlBuilder.getURL();
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(RateObject.class, new RatesDeserializer())
                    .create();
            ApiResponse apiResponse = gson.fromJson(in, ApiResponse.class);
            return new ExchangeOutput(apiResponse);
        } catch (URISyntaxException | IOException e) {
            return new ExchangeOutput("Connection Failed");
        }
    }

    private static class URLBuilder {
        private StringBuilder folders, params;
        private String connType, host;

        void setConnectionType(String conn) {
            connType = conn;
        }

        URLBuilder() {
            folders = new StringBuilder();
            params = new StringBuilder();
        }

        URLBuilder(String host) {
            this();
            this.host = host;
        }

        void addSubfolder(String folder) {
            folders.append("/");
            folders.append(folder);
        }

        void addParameter(String parameter, String value) {
            if (params.toString().length() > 0) {
                params.append("&");
            }
            params.append(parameter);
            params.append("=");
            params.append(value);
        }

        URL getURL() throws URISyntaxException, MalformedURLException {
            URI uri = new URI(connType, host, folders.toString(),
                    params.toString(), null);
            return uri.toURL();
        }
    }
}