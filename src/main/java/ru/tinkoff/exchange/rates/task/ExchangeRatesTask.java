package ru.tinkoff.exchange.rates.task;

import ru.tinkoff.exchange.rates.cache.CacheAdapter;
import ru.tinkoff.exchange.rates.cache.HttpReaderCacheAdapter;
import ru.tinkoff.exchange.rates.cache.ReaderCacheAdapter;
import ru.tinkoff.exchange.rates.dto.ExchangeOutput;
import ru.tinkoff.exchange.rates.enumeration.Currency;
import ru.tinkoff.exchange.rates.model.ApiResponse;
import ru.tinkoff.exchange.rates.service.HttpReaderStrategyImpl;
import ru.tinkoff.exchange.rates.util.NetworkUtils;

import java.net.InetAddress;
import java.util.concurrent.Callable;

import static ru.tinkoff.exchange.rates.util.NetworkUtils.isAvailableNetworkInterfaces;

public class ExchangeRatesTask implements Callable<ExchangeOutput> {
    private final Currency fromCurrency;
    private final Currency toCurrency;

    public ExchangeRatesTask(Currency fromCurrency, Currency toCurrency) {
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
    }

    @Override
    public ExchangeOutput call() {
        CacheAdapter adapter;
        if (isAvailableNetworkInterfaces()) {
            adapter = new HttpReaderCacheAdapter(new HttpReaderStrategyImpl());
        } else {
            adapter = new ReaderCacheAdapter();
        }
        return adapter.read(fromCurrency, toCurrency);
    }
}
