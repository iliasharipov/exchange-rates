package ru.tinkoff.exchange.rates.cache;

import ru.tinkoff.exchange.rates.model.ApiCache;
import ru.tinkoff.exchange.rates.model.ApiResponse;

public interface CacheManager {
    void put(String cacheKey, ApiResponse response);

    ApiCache get(String cacheKey);

    void clear(String cacheKey);
}