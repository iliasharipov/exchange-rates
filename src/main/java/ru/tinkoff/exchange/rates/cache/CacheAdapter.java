package ru.tinkoff.exchange.rates.cache;

import ru.tinkoff.exchange.rates.dto.ExchangeOutput;
import ru.tinkoff.exchange.rates.enumeration.Currency;
import ru.tinkoff.exchange.rates.service.ReaderStrategy;

public abstract class CacheAdapter implements ReaderStrategy {
    abstract ExchangeOutput invalidateCache(Currency fromCurrency, Currency toCurrency);

    public String generateCacheKey(Currency fromCurrency, Currency toCurrency) {
        return fromCurrency.name() + toCurrency.name();
    }
}