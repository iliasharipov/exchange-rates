package ru.tinkoff.exchange.rates.cache;

import com.google.gson.Gson;
import ru.tinkoff.exchange.rates.model.ApiCache;
import ru.tinkoff.exchange.rates.model.ApiResponse;

import java.io.*;

public class FileCacheManager implements CacheManager {

    private static FileCacheManager instance;
    private static final Object monitor = new Object();
    private static final String FILENAME = "cache";
    private Gson gson;

    private FileCacheManager() {
        gson = new Gson();
    }

    public void put(String cacheKey, ApiResponse response) {
        ApiCache cache = new ApiCache(cacheKey, response);
        try (PrintWriter inFileWriter = new PrintWriter(new FileWriter(FILENAME, true))) {
            ApiCache fromCache = get(cacheKey);
            if (fromCache != null) {
                clear(fromCache.getKey());
            }
            inFileWriter.println(gson.toJson(cache));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ApiCache get(String cacheKey) {
        String currentLine;
        File inFile = new File(FILENAME);
        if (!inFile.exists()) {
            try {
                inFile.createNewFile();
            } catch (IOException e) {
                return null;
            }
        }
        try (BufferedReader inFileReader = new BufferedReader(new FileReader(inFile))) {
            while ((currentLine = inFileReader.readLine()) != null) {
                ApiCache fromCache = gson.fromJson(currentLine, ApiCache.class);
                if (fromCache != null) {
                    if (cacheKey.equalsIgnoreCase(fromCache.getKey())) {
                        return fromCache;
                    }
                }
            }
        } catch (IOException e) {
            return null;
        }
        return null;
    }

    public void clear(String cacheKey) {
        File inFile = new File(FILENAME);
        File tempFile = new File(inFile.getName() + ".tmp");
        String currentLine;
        try (BufferedReader inFileReader = new BufferedReader(new FileReader(FILENAME));
             PrintWriter tempFileWriter = new PrintWriter(new FileWriter(tempFile, true))
        ) {
            while ((currentLine = inFileReader.readLine()) != null) {
                ApiCache fromCache = gson.fromJson(currentLine, ApiCache.class);
                if (cacheKey.equalsIgnoreCase(fromCache.getKey())) continue;
                tempFileWriter.println(currentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        inFile.delete();
        tempFile.renameTo(inFile);

    }

    public static FileCacheManager getInstance() {
        if (instance == null) {
            synchronized (monitor) {
                if (instance == null) {
                    instance = new FileCacheManager();
                }
            }
        }
        return instance;
    }
}