package ru.tinkoff.exchange.rates.cache;

import ru.tinkoff.exchange.rates.dto.ExchangeOutput;
import ru.tinkoff.exchange.rates.enumeration.Currency;
import ru.tinkoff.exchange.rates.model.ApiCache;
import ru.tinkoff.exchange.rates.model.ApiResponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class ReaderCacheAdapter extends CacheAdapter {
    @Override
    public ExchangeOutput invalidateCache(Currency fromCurrency, Currency toCurrency) {
        CacheManager cacheManager = FileCacheManager.getInstance();
        String cacheKey = generateCacheKey(fromCurrency, toCurrency);
        ApiCache fromCache = cacheManager.get(cacheKey);
        ApiResponse apiResponse = null;
        if (fromCache != null) {
            LocalDateTime cachedDate = LocalDate.parse(fromCache.getApiResponse().getDate()).atTime(0, 0);
            LocalDateTime today = LocalDate.now().atTime(0, 0);
            LocalDateTime yesterday = today.minus(Period.ofDays(1));
            LocalDateTime ratesUpdateTime = today.withHour(18).withMinute(0);
            if (cachedDate.equals(today) || (cachedDate.equals(yesterday) && today.isAfter(ratesUpdateTime))) {
                return new ExchangeOutput(fromCache.getApiResponse());
            } else {
                cacheManager.clear(cacheKey);
            }
        }
        return new ExchangeOutput(apiResponse);
    }

    @Override
    public ExchangeOutput read(Currency fromCurrency, Currency toCurrency) {
        ExchangeOutput output = invalidateCache(fromCurrency, toCurrency);
        if (output != null && output.getApiResponse() != null) {
            return output;
        } else {
            return new ExchangeOutput("No Exchange Rate In Cache For Pair: " + fromCurrency.name() + "/" + toCurrency.name());
        }
    }
}
