package ru.tinkoff.exchange.rates.cache;

import ru.tinkoff.exchange.rates.dto.ExchangeOutput;
import ru.tinkoff.exchange.rates.enumeration.Currency;
import ru.tinkoff.exchange.rates.model.ApiCache;
import ru.tinkoff.exchange.rates.model.ApiResponse;
import ru.tinkoff.exchange.rates.service.ReaderStrategy;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class HttpReaderCacheAdapter extends CacheAdapter {
    private final ReaderStrategy strategy;

    public HttpReaderCacheAdapter(ReaderStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public ExchangeOutput read(Currency fromCurrency, Currency toCurrency) {
        return invalidateCache(fromCurrency, toCurrency);
    }

    @Override
    public ExchangeOutput invalidateCache(Currency fromCurrency, Currency toCurrency) {
        CacheManager cacheManager = FileCacheManager.getInstance();
        String cacheKey = generateCacheKey(fromCurrency, toCurrency);
        ApiCache fromCache = cacheManager.get(cacheKey);
        ExchangeOutput output;
        if (fromCache != null) {
            ApiResponse apiResponse = fromCache.getApiResponse();
            if (apiResponse != null && apiResponse.getDate() != null && apiResponse.getRates() != null) {
                LocalDateTime cachedDate = LocalDate.parse(fromCache.getApiResponse().getDate()).atTime(0, 0);
                LocalDateTime today = LocalDate.now().atTime(0, 0);
                LocalDateTime yesterday = today.minus(Period.ofDays(1));
                if (cachedDate.equals(yesterday) || (cachedDate.equals(today))) {
                    return new ExchangeOutput(fromCache.getApiResponse());
                } else {
                    cacheManager.clear(cacheKey);
                }
            }
        }
        output = strategy.read(fromCurrency, toCurrency);
        if (output != null && output.getApiResponse() != null) {
            cacheManager.put(cacheKey, output.getApiResponse());
            return output;
        } else {
            return new ExchangeOutput("Bad http response");
        }
    }
}